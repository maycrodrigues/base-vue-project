const path = require('path');

module.exports = {
  assetsDir: 'assets',
  configureWebpack: {
    resolve: {
      alias: {
        styles: path.resolve(__dirname, './src/assets/scss'),
        nodemodules: path.resolve(__dirname, "./node_modules")
      }
    }
  }
}