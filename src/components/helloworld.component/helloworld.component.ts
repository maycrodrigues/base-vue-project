import { Component, Vue, Prop } from 'vue-property-decorator';

@Component({
  components: {},
})
export default class HelloWorldComponent extends Vue {

  @Prop() private msg!: string;

  constructor() {
    super();
  }

  mounted() { }

}
