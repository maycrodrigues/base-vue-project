import { Component, Vue } from 'vue-property-decorator';
import HelloWorld from '@/components/helloworld.component/index.vue'; // @ is an alias to /src

@Component({
  components: {
    HelloWorld,
  },
})
export default class Intro extends Vue {

  constructor() {
    super();
  }

  mounted() { }

}
